# coding: utf-8
from __future__ import unicode_literals
import re 
import requests
import json
import msgbox
import sys
from vars import *

def context():

	global desktop
	global model
	global active_sheet

	desktop = XSCRIPTCONTEXT.getDesktop()
	model = desktop.getCurrentComponent()

	# accessing the active sheet
	active_sheet = model.CurrentController.ActiveSheet

def cell_range():
	'''Selected cell range to provision'''
	
	#sheet = model.CurrentController.getActiveSheet()
	range_source = model.getCurrentSelection()
	data_source = range_source.getRangeAddress()
	first_row = data_source.StartRow
	last_row = data_source.EndRow
	first_col = data_source.StartColumn
	first_row = data_source.StartRow
	last_col = data_source.EndColumn
	range_cell = active_sheet.getCellRangeByPosition(first_col, first_row, last_col, last_row)
	pbx_config = range_cell.getDataArray()
	return pbx_config

def message_button(*args):
	'''Message box'''

	message = msgbox.MsgBox(XSCRIPTCONTEXT.getComponentContext())
	#message.renderFromBoxSize(130)
	message.renderFromButtonSize()
	message.numberOflines = 2
	message.addButton("OK")

	if args[0] == 'zone' and args[1] != 200:
		message.show('DNS A record can\'t be created',0,'Status')
	elif args[0] == 'zone' and args[1] == 200:
		message.show('DNS A record created',0,'Status')
	elif args[0] and args[1] == 200:
		message.show(args[2] + ' created and updated',0,'Status')
	elif not args[0] and args[1] == 200:
		message.show(args[2] + ' settings updated',0,'Status')
	else:
		message.show(args[2] + ' provisioning failed',0,'Status')

def add_dns(*args):
	'''Adding A record'''

	context()
	range_source = active_sheet.getCellRangeByPosition(0, 0, 1, 0)
	data_source = range_source.getDataArray()
	hostname = re.sub('[\W_]', '',data_source[0][1])
	headers = {'Authorization': 'Bearer ' + API_CLOUDFLARE_TOKEN}
	payload = {'type':'A','name':hostname + '.pbx','content':PBX_IP,'ttl':'600','proxied':False}
	r = requests.post(url = API_CLOUDFLARE_ENDPOINT + 'zones/' + CLOUDFLARE_ZONE_ID + '/dns_records', data = json.dumps(payload), headers = headers)
	if (r.status_code != 200):
		message_button('zone', r.status_code)
	message_button('zone', r.status_code)

def domain(*args):
	'''Domain provisioing'''

	context()
	#range_source = active_sheet.getCellRangeByPosition(0, 0, 1, 20)
	#data_source = range_source.getDataArray()
	#pbx_config = dict(zip(data_source[0],data_source[1]))
	pbx_config = dict(cell_range())
	hostname = re.sub('[\W_]', '',pbx_config['domain_name'])
	domain_name = hostname.lower() + PBX_DOMAIN
	URL_CREATE = "rest/system/domains"
	URL_UPDATE = "rest/domain/"
	new_domain = False
	r = requests.get(url = API_ENDPOINT + URL_UPDATE + domain_name + '/users', auth = (API_USER,API_KEY))
	if (r.status_code != 200):
		payload = [domain_name,hostname.lower()]
		r = requests.post(url = API_ENDPOINT + URL_CREATE, data = json.dumps(payload),auth = (API_USER,API_KEY))
		new_domain = True
	pbx_config.pop('domain_name')
	r = requests.post(url = API_ENDPOINT + URL_UPDATE + domain_name + '/config', data = json.dumps(pbx_config), auth = (API_USER,API_KEY))
	message_button(new_domain, r.status_code, 'Domain')

def general_settings(*args):
	'''General settings and Voicemail provisioing'''

	context()
	pbx_config = cell_range()
	hostname = re.sub('[\W_]', '',pbx_config[0][1])
	domain_name = hostname.lower() + PBX_DOMAIN
	URL_UPDATE = "rest/domain/"
	pbx_config = dict(pbx_config)
	r = requests.post(url = API_ENDPOINT + URL_UPDATE + domain_name + '/settings', data = json.dumps(pbx_config), auth = (API_USER,API_KEY))
	message_button(True, r.status_code, 'Settings')

def create_extensions(*args):
    """Extensions provisioning"""

    context()
    pbx_config = cell_range()
    extensions_type = args[0]

    #pbx_config = dict(zip(pbx_config[0],pbx_config[1]))
    hostname = re.sub('[\W_]', '',pbx_config[0][1])
    domain_name = hostname.lower() + PBX_DOMAIN
    URL_CREATE = 'rest/domain/' + domain_name + '/addacc'
    URL_UPDATE = 'rest/domain/' + domain_name + '/user_settings/'

    #checking extension status and creating it if non-existent
    for i in pbx_config[1][1:]:
    	#extension = {'type':'attendants', 'account': [k for k in pbx_config[1][1:]]}
    	r = requests.get(url = API_ENDPOINT + URL_UPDATE + i, auth = (API_USER,API_KEY))
    	if (r.status_code != 200) and extensions_type == 'extensions':
    		extension = {'type':extensions_type,'account_ext':i}
    		r = requests.post(url = API_ENDPOINT + URL_CREATE, data = json.dumps(extension),auth = (API_USER,API_KEY))
    	elif (r.status_code != 200) and extensions_type != 'extensions':
    		extension = {'type':extensions_type,'account':i}
    		r = requests.post(url = API_ENDPOINT + URL_CREATE, data = json.dumps(extension),auth = (API_USER,API_KEY))
    	pbx_config1 = {k[0]:k[pbx_config[1].index(i)] for k in pbx_config[1:]}
    	pbx_config1.update({'type':extensions_type})
    	r = requests.post(url = API_ENDPOINT + URL_UPDATE + i, data = json.dumps(pbx_config1),auth = (API_USER,API_KEY))
    	if r.status_code != 200:
    		message_button(i, r.status_code, 'Ext. ' + i)
    		break
    message_button(True, r.status_code, 'Ext.(s)')

def service_flags(*args):
    """Service flags provisioning"""
    
    create_extensions('srvflags')

def ivr(*args):
    """IVR provisioning"""

    create_extensions('attendants')

def hunt_groups(*args):
	"""Hunt groups provisioning"""

	create_extensions('hunts')

def extensions(*args):
    """User extensions provisioning"""

    create_extensions('extensions')

def agent_groups(*args):
	"""Agent groups provisioning"""

	create_extensions('acds')
	
def voip_phones(*args):
	"""VoIP phones provisioning"""

	context()
	pbx_config = cell_range()
	hostname = re.sub('[\W_]', '',pbx_config[0][1])
	domain_name = hostname.lower() + PBX_DOMAIN
	URL_CREATE = 'rest/system/prov_phones'

	for i in pbx_config[1][1:]:
		pbx_config1 = {k[0]:k[pbx_config[1].index(i)] for k in pbx_config[1:]}
		pbx_config1.update({'domain':domain_name})
		r = requests.post(url = API_ENDPOINT + URL_CREATE, data = json.dumps(pbx_config1),auth = (API_USER,API_KEY))
		if r.status_code != 200:
			message_button(i, r.status_code, 'Phone ' + i)
			break
	message_button(True, r.status_code, 'Phone(s)')

def did(*args):
	"""DID provisioning"""

	context()
	pbx_config = cell_range()
	hostname = re.sub('[\W_]', '',pbx_config[0][1])
	domain_name = hostname.lower() + PBX_DOMAIN
	URL_CREATE = 'rest/domain/' + domain_name + '/did'

	for i in pbx_config[1][1:]:
		pbx_config1 = {k[0]:k[pbx_config[1].index(i)] for k in pbx_config[1:]}
		pbx_config1.update({'cmd':'add'})
		r = requests.post(url = API_ENDPOINT + URL_CREATE, data = json.dumps(pbx_config1),auth = (API_USER,API_KEY))
		if r.status_code != 200:
			message_button(i, r.status_code, 'DID ' + i)
			break
	message_button(True, r.status_code, 'DID(s)')

def trunks(*args):
	"""Trunks provisioning"""

	context()
	pbx_config = cell_range()
	hostname = re.sub('[\W_]', '',pbx_config[0][1])
	domain_name = hostname.lower() + PBX_DOMAIN
	URL_CREATE = 'rest/domain/' + domain_name
	#retreiving trunks list
	r = requests.get(url = API_ENDPOINT + URL_CREATE + "/domain_trunks", auth = (API_USER,API_KEY))
	trunks=r.json()
	trunks1 =[name['name'] for name in trunks]

	for i in pbx_config[1][1:]:
		if i not in trunks1:
			pbx_config1 = {k[0]:k[pbx_config[1].index(i)] for k in pbx_config[1:3]}
			r = requests.post(url = API_ENDPOINT + URL_CREATE + "/domain_trunks", data = json.dumps(pbx_config1), auth = (API_USER,API_KEY))
			if r.status_code != 200:
				message_button(i, r.status_code, 'Trunk(s) ' + i)
				break
			trunk_id = r.text
			pbx_config1 = {k[0]:k[pbx_config[1].index(i)] for k in pbx_config[1:]}
			r = requests.post(url = API_ENDPOINT + URL_CREATE + "/edit_trunk/" + trunk_id, data = json.dumps(pbx_config1), auth = (API_USER,API_KEY))
		else:
			trunk_id = trunks[trunks1.index(i)]['id']
			pbx_config1 = {k[0]:k[pbx_config[1].index(i)] for k in pbx_config[1:]}
			r = requests.post(url = API_ENDPOINT + URL_CREATE + "/edit_trunk/" + trunk_id, data = json.dumps(pbx_config1), auth = (API_USER,API_KEY))
	message_button(True, r.status_code, 'Trunk(s)')